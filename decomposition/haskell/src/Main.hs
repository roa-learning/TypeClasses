module Main where

-- View of lumberjack
class Tree t where assessedValue :: t -> Integer
class Tree t => HardWood t
class Tree t => SoftWood t

-- View of the bird
class Plant p where foodValue :: p -> Integer
class Plant p => NectarPlant p where sweetness :: p -> Integer
class Plant p => InsectPant p

-- Separate classification hierarchies
data Cherry = Cherry Integer

-- Lumberjack view
instance Tree Cherry where assessedValue (Cherry s) = s * 42
instance HardWood Cherry

-- Bird view
instance Plant Cherry where foodValue (Cherry s) = s * 23
instance NectarPlant Cherry where sweetness (Cherry s) = s * 99

main :: IO ()
main = do
  putStrLn "Tyranny of the dominant decomposition!"
