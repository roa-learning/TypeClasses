package roa.tc.decomposition

/**
 * Created by roa on 30/10/15.
 */

trait Tree {
  def assessedValue: Int
}

trait HardWood extends Tree {}
trait SoftWood extends Tree {}

trait Plant {
  def foodValue: Int
}

trait NectarPlant extends Plant {}
trait InsectPlant extends Plant {}

case class Cherry(size: Int)

object Cherry {
  implicit def asTree(c: Cherry): Tree = new Tree {
    override def assessedValue = c.size * 42
  }

  implicit def asHardWood(c: Cherry): HardWood = new HardWood {
    override def assessedValue: Int = asTree(c).assessedValue
  }

  implicit def asPlant(c: Cherry): Plant = new Plant {
    override def foodValue: Int = c.size * 23
  }

  implicit def asNectarPlant(c: Cherry): NectarPlant = new NectarPlant {
    override def foodValue: Int = asPlant(c).foodValue
  }
}