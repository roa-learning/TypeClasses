trait Expression {
  def eval: Int
}

class Literal(value: Int) extends Expression {
  override def eval: Int = value
}

class Add(l: Expression, r: Expression) extends Expression {
  override def eval: Int = l.eval + r.eval
}

// Adding a new data type is easy

class Multiply(l: Expression, r: Expression) extends Expression {
  override def eval: Int = l.eval * r.eval
}

/*
 Adding a new operation can be done by adding a new method to all
 the existing classes, thus modifying existing code.
*/