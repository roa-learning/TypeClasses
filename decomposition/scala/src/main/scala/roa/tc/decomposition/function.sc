sealed trait Expression

case class Literal(l: Int) extends Expression

case class Add(l: Expression, r: Expression) extends Expression

object Expression {
  def eval(e: Expression): Int = e match {
    case Literal(l) => l
    case Add(l, r) => eval(l) + eval(r)
  }

  // Adding a new operation ofer the data structure is easy
  def view(e: Expression): String = e match {
    case Literal(l) => l.toString
    case Add(l, r) => l.toString + " + " + r.toString
  }
}

/*
  Adding a new 'multiply' data extension requires modifying the definition of the case classes which can only be done in this file.
*/