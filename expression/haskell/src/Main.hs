module Main where

data Literal = Literal Int deriving (Show)
data Add l r = Add l r deriving (Show)

class Eval x where
  eval :: x -> Int

instance Eval Literal where
  eval (Literal x) = x

instance (Eval l, Eval r) => Eval (Add l r) where
  eval (Add l r) = eval l + eval r

-- Now we add the multiply type.
data Multiply l r = Multiply l r deriving (Show)

instance (Eval l, Eval r) => Eval (Multiply l r) where
  eval (Multiply l r) = eval l * eval r

class Print x where
  pprint :: x -> String

instance Print Literal where
  pprint (Literal x) = show x

instance (Print l, Print r) => Print (Multiply l r) where
  pprint (Multiply l r) = "(" ++ pprint l ++ " * " ++ pprint r ++ ")"

instance (Print l, Print r) => Print (Add l r) where
  pprint (Add l r) = "(" ++ pprint l ++ " + " ++ pprint r ++ ")"

{- List Specialization -}

class EfficientList a where
  data EList a
  create :: a -> EList a
  add :: EList a -> a -> EList a

instance EfficientList Int where
  data EList Int = IntList [Int] deriving (Show)
  create e = IntList [e]
  add (IntList t) e = IntList (e:t)

instance EfficientList Bool where
  data EList Bool = BoolList [Bool] deriving (Show)
  create e = BoolList [e]
  add (BoolList t) e = BoolList (e:t)

main :: IO ()
main = do
  putStrLn $ "Evaluation result: " ++ show (eval (Add (Add (Literal 5) (Literal 6)) (Literal 8)))
  putStrLn $ "Evaluation result: " ++ show (eval (Multiply (Add (Literal 5) (Literal 6)) (Literal 8)))
  putStrLn $ "Evaluation result: " ++ show (pprint (Multiply (Add (Literal 5) (Literal 6)) (Literal 8)))
  print ("Create int list: " ++ show (create (5 :: Int)))
  print ("Create bool list: " ++ show (create True))
