trait EfficientList[T] {
  def create: EfficientList[T]
  def add(e: T): EfficientList[T]
}

class InefficientList[T] extends EfficientList[T] {
  override def create: EfficientList[T] = {
    println("Default implementation")
    this
  }

  override def add(e: T): EfficientList[T] = this
}

class EfficientBooleanList extends EfficientList[Boolean] {
  override def create: EfficientList[Boolean] = {
    println("Add boolean")
    this
  }

  override def add(e: Boolean): EfficientList[Boolean] = this
}

class EfficientIntegerList extends EfficientList[Int] {
  override def create: EfficientList[Int] = {
    println("Add integer")
    this
  }

  override def add(e: Int): EfficientList[Int] = this
}

implicit def newBooleanList: EfficientList[Boolean] =
  new EfficientBooleanList

implicit def newIntegerList: EfficientList[Int] =
 new EfficientIntegerList

def createListFrom[T](v: T)(implicit nList: EfficientList[T] = null): EfficientList[T] = {
  if (nList != null) nList else new InefficientList }


createListFrom(false).create
createListFrom(10).create
createListFrom(10d).create
