case class Literal(value: Int)

case class Add[A, B](left: A, right: B)

trait EvalLike[E] {
  def eval(e: E): Int
}

implicit def literalEval = new EvalLike[Literal] {
  override def eval(l: Literal): Int = l.value
}

implicit def addEval[A, B](implicit e1: EvalLike[A], e2: EvalLike[B]) = new EvalLike[Add[A, B]] {
  def eval(t: Add[A, B]) = e1.eval(t.left) + e2.eval(t.right)
}

def evaluateExpression[A](exp: A)(implicit e: EvalLike[A]) = e.eval(exp)

evaluateExpression(Literal(5))
evaluateExpression(Add(Literal(1), Literal(2)))
evaluateExpression(Add(Add(Literal(1), Literal(2)), Literal(3)))

/**
  Now we add a Multiplication expression type
  (Extension in the data domanin)
  */

case class Multiply[A, B](left: A, right: B)

implicit def multiplyEval[A, B](implicit e1: EvalLike[A], e2: EvalLike[B]) = new EvalLike[Multiply[A, B]] {
  override def eval(e: Multiply[A, B]): Int = e1.eval(e.left) * e2.eval(e.right)
}

evaluateExpression(Multiply(Add(Literal(3), Literal(4)), Literal(7)))

/**
  Now we add a new function(ality) for printing out expressions
  */

trait PrintLike[P] {
  def print(p: P): String
}

implicit def literalPrint = new PrintLike[Literal] {
  override def print(p: Literal): String = p.value.toString
}

implicit def addPrint[A, B](implicit e1: PrintLike[A], e2: PrintLike[B]) = new PrintLike[Add[A, B]] {
  def print(t: Add[A, B]) = "(" + e1.print(t.left) + " + " + e2.print(t.right) + ")"
}

implicit def multiplyPrint[A, B](implicit e1: PrintLike[A], e2: PrintLike[B]) = new PrintLike[Multiply[A, B]] {
  def print(t: Multiply[A, B]) = "(" + e1.print(t.left) + " * " + e2.print(t.right) + ")"
}

def printExpression[A](exp: A)(implicit e: PrintLike[A]) = e.print(exp)

printExpression(Literal(5))
printExpression(Add(Literal(1), Literal(2)))
printExpression(Add(Add(Literal(1), Literal(2)), Literal(3)))
printExpression(Multiply(Add(Literal(3), Literal(4)), Literal(7)))
