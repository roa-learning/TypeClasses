package roa.tc.adapter.java;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by roa on 27/10/15.
 */
public class AdapterManager {
    private static final class Key {
        public final Class<?> from;
        public final Class<?> to;
        private final String key;

        private Key(Class<?> from, Class<?> to) {
            this.from = from;
            this.to = to;
            key = from.getCanonicalName() + " --> " + to.getCanonicalName();
        }

        @Override
        public boolean equals(Object other) {
            if (!(other instanceof Key))
                return false;
            Key otherKey = (Key) other;
            return key.equals(otherKey.key);
        }

        @Override
        public int hashCode() {
            return key.hashCode();
        }
    }

    private Map<Key, Adapter<?, ?>> registry = new HashMap<>();

    public <From, To> void addAdaptor(Class<From> from, Class<To> to, Adapter<From, To> adapter) {
        registry.put(new Key(from, to), adapter);
    }

    public <From, To> To adapt(From from, Class<To> to) {
        return ((Adapter<From, To>) registry.get(new Key(from.getClass(), to))).adapt(from);
    }
}
