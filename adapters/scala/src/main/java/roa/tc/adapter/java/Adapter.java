package roa.tc.adapter.java;

/**
 * Created by roa on 27/10/15.
 */
public interface Adapter<From, To> {
    To adapt(From f);
}
