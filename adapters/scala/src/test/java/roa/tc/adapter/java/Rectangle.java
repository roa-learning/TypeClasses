package roa.tc.adapter.java;

/**
 * Created by roa on 27/10/15.
 */
public class Rectangle {
    public final int x;
    public final int y;
    public final int width;
    public final int height;

    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}
