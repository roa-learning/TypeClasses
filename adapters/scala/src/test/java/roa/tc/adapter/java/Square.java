package roa.tc.adapter.java;

/**
 * Created by roa on 27/10/15.
 */
public class Square {
    public final int x;
    public final int y;
    public final int size;

    public Square(int x, int y, int size) {
        this.x = x;
        this.y = y;
        this.size = size;
    }
}
