package roa.tc.adapter.java;

/**
 * Created by roa on 27/10/15.
 */
public interface Eq<T> {
    boolean eq(T other);
}
