package roa.tc.adapter.java;

/**
 * Created by roa on 27/10/15.
 */
public class RectangleToEqAdapter implements Adapter<Rectangle, Eq> {
    @Override
    public Eq<Rectangle> adapt(Rectangle r) {
        return new Eq<Rectangle>() {
            @Override
            public boolean eq(Rectangle o) {
                return r.x == o.x && r.y == o.y && r.width == o.width && r.height == o.height;
            }
        };
    }
}
