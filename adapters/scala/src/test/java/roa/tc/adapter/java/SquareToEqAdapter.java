package roa.tc.adapter.java;

/**
 * Created by roa on 27/10/15.
 */
public class SquareToEqAdapter implements Adapter<Square, Eq> {
    @Override
    public Eq<Square> adapt(Square f) {
        return new Eq<Square>() {
            @Override
            public boolean eq(Square other) {
                return f.x == other.x && f.y == other.y && f.size == other.size;
            }
        };
    }
}
