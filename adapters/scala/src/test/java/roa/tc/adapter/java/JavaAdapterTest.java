package roa.tc.adapter.java;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by roa on 27/10/15.
 */
public class JavaAdapterTest {

    private AdapterManager m = new AdapterManager();
    private static final Square square = new Square(0, 0, 10);
    private static final Rectangle rectangle = new Rectangle(0, 0, 0, 10);

    @Before
    public void setUp() {
        m.addAdaptor(Square.class, Eq.class, new SquareToEqAdapter());
        m.addAdaptor(Rectangle.class, Eq.class, new RectangleToEqAdapter());
    }

    @Test
    public void useDefinedAdapter() {
        assertTrue(m.adapt(square, Eq.class).eq(square));
        assertTrue(m.adapt(rectangle, Eq.class).eq(rectangle));
    }

    @Test
    public void useUnknownAdapter() {
        try {
            assertFalse(m.adapt("Some string", Eq.class).eq("Some string"));
            fail();
        } catch (NullPointerException e) {
        }
    }

    @Test
    public void typeSafeAdapter() {
        assertTrue(new RectangleToEqAdapter().adapt(rectangle).eq(rectangle));
        assertTrue(new SquareToEqAdapter().adapt(square).eq(square));
//        assertTrue(new ???().adapt("Some string").eq("Some string"));
    }
}
