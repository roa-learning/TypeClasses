package roa.tc.adapter.scala

import org.junit.{Before, Test}
import org.junit.Assert._

/**
 * Created by roa on 27/10/15.
 */

trait Eq[T] { def newEquals(other: T): Boolean }

case class Square(x: Int, y: Int, size: Int)

object Square {
  implicit def squareToEq(s: Square): Eq[Square] = new Eq[Square] {
    override def newEquals(o: Square): Boolean =
      s.x == o.x && s.y == o.y && s.size == o.size
  }
}

case class Rectangle(x: Int, y: Int, width: Int, height: Int)

object Rectangle {
  implicit def rectangleToEq(r: Rectangle): Eq[Rectangle] = new Eq[Rectangle] {
    override def newEquals(o: Rectangle): Boolean =
      r.x == o.x && r.y == o.y && r.width == o.width && r.height == o.height
  }
} 

class AdapterTest {

  private val square: Square = new Square(0, 0, 10)
  private val rectangle: Rectangle = new Rectangle(0, 0, 0, 10)

  @Test
  def useDefinedAdapter() = {
    assertTrue(square.eq(square))
    assertTrue(rectangle.eq(rectangle))
  }

  @Test
  def useUnknownAdapter() = {
//    assertTrue("Some string".newEquals("Some string"))
  }

}
