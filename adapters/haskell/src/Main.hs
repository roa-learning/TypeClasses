module Main where

data Square = Square Int Int Int
data Rectangle = Rectangle Int Int Int Int

class Coords a where
  x :: a -> Int
  y :: a -> Int

class Size a where
  height :: a -> Int
  width :: a -> Int

class NewEquals a where
  newEquals :: a -> a -> Bool

instance Coords Square where
  x (Square sx _ _) = sx
  y (Square _ sy _) = sy

instance Size Square where
  height (Square _ _ size) = size
  width (Square _ _ size) = size

instance Coords Rectangle where
  x (Rectangle rx _ _ _) = rx
  y (Rectangle _ ry _ _) = ry

instance Size Rectangle where
  height (Rectangle _ _ rh _) = rh
  width (Rectangle _ _ _ rw) = rw

instance NewEquals Rectangle where
  newEquals r1 r2 = x r1 == x r2 && y r1 == y r2 &&
                            height r1 == height r2 && width r1 == width r2

instance NewEquals Square where
  newEquals s1 s2 = x s1 == x s2 && y s1 == y s2 &&
                    height s1 == height s2 && width s1 == width s2

-- instance (Coords a, Size a) => NewEquals a where
--   newEquals shape1 shape2 = x shape1 == x shape2 && y shape1 == y shape2 &&
--                             height shape1 == height shape2 && width shape1 == width shape2

main :: IO ()
main = do
  let square = Square 0 0 10
  let rectangle = Rectangle 0 0 10 10
  print ("Square equals itself: " ++ show (newEquals square square))
  print ("Rectangle equals itself: " ++ show (newEquals rectangle rectangle))
--  print ("String equals itself: " ++ show (newEquals "Some string" "Some string"))
